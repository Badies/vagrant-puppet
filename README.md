# Vagrant-puppet

Create puppet environment in 5 minutes  

Minimum requirements:

-RAM 6G  
-DISK 30G  
-Happiness 1000000G  

We have few steps that need to be done before firing up the puppet env :  

1. Add the Centos 7.4 to your Vagrant setup:  

vagrant box add centos/7 --box-version 1803.01 --provider virtualbox  

To read more about this version go to :  
https://app.vagrantup.com/centos/boxes/7/versions/1803.01  

To list all boxes run:

vagrant box list
